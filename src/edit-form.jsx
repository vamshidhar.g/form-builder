/**
  * <EditForm />
  */

 import React from 'react';

 const JSON_String = '{"name":"registration form","id":123,"description":"","styles":{},"pages":[{"sequence":0,"elements":[],"sections":[{"sequence":0,"id":"B0E65A10-C9C8-4E49-8C83-504C82CC2935","name":"table_input_22467287-0619-4712-BBE8-0CA55F37B2AF","label":"Personal Details ","field_varient":"outlined","title":"Section Header","element":"SectionHeader","elements":[{"sequence":1,"id":"41EC7FBB-CF1A-409E-A9F8-A94D0C77EC23","name":"text_input_EB07F28E-68E2-483C-8B2F-7F8EFB31C4D7","label":"Full Name ","field_varient":"filled","title":"Text (Material)","element":"MaterialText","options":{"required":false}},{"sequence":2,"id":"1ECBDCE6-703D-4BE0-9A7C-52CBDD0FEE38","name":"number_input_BEA4BDE3-6622-4287-980E-A272BD67F5B4","label":"Age ","field_varient":"outlined","title":"Number (Material)","element":"MaterialNumber","options":{"required":true}},{"sequence":3,"id":"A184A862-91E9-441C-8AA9-2B23455E1B4C","name":"text_input_7B18FA81-EDBD-4DA3-953A-02F437AA1A2A","label":"Phone","field_varient":"outlined","title":"Phone (Material)","element":"MaterialPhone","options":{"required":true,"isMasked":false}}]},{"sequence":1,"id":"F622FFBC-B888-4469-97F2-E84FEFDD67A4","name":"table_input_DBDC25D8-DAF1-43FD-9375-8800390E55A1","label":"Career Background ","field_varient":"outlined","title":"Section Header","element":"SectionHeader","elements":[{"sequence":5,"id":"791D74AE-BD33-4CDC-A8C9-AD37AB8E2295","name":"table_input_D7541D89-2AE7-4735-B3B6-C3C6BF64C5AB","label":"Educational Background ","field_varient":"outlined","title":"Input Table (Material)","element":"MaterialTable","headerList":[{"headerId":0,"label":"Type of Activity","required":false},{"headerId":3,"label":"High School","required":false},{"headerId":1,"label":"Graduation","required":true},{"headerId":2,"label":"Post Graduation","required":true}],"rows":[{"rowId":0},{"rowId":1},{"rowId":2}],"options":{"required":false}}]}]}]}';
 
 export default class EditForm extends React.Component {
    constructor(props) {
        super(props);
    }
 
    generateDataItems(){
        //lert("Generate Data Items");
        const json = JSON.parse(localStorage.getItem('FORM_JSON'));
        let formElements = [];

        json.pages.forEach( page => {
           
            // check if elements exists in the page object
            if(page.elements.length){
                formElements = formElements.concat(page.elements);
            }

           // check if page has sections or not
            if(page.hasOwnProperty("sections") && page.sections.length){
                page.sections.forEach( section => {
                    formElements.push(this.getSectionObject(section, json.styles));
                   
                    //check of the section has elements or not
                    if(section.elements.length)
                        formElements = formElements.concat(section.elements);
                })
            }
        })

       formElements = this.generatePreviewItems(formElements, json.styles);

       this.props.editExistingForm("Extracted the elements", formElements);
    }

    generatePreviewItems(list, globalStyles){
        console.log(list);

        let newList = [];

        list.forEach( item => {
            switch(item.element){
                case 'MaterialText':
                    newList.push(this.getText(item, globalStyles));
                    break;
                case 'MaterialPhone':
                    newList.push(this.getPhone(item, globalStyles));
                    break;
                case 'MaterialNumber':
                    newList.push(this.getNumber(item, globalStyles));
                    break;
                case 'SectionHeader':
                    newList.push(item);
                    break;
                case 'MaterialTable':
                    newList.push(this.getTable(item, globalStyles));
                    break;
                default:
                    break;
            }
        })

        console.log("New List");
        console.log(newList);
        return newList;
    }

    setCommonFields(newObject, sourceObject, globalStyles){
       newObject.id = sourceObject.id;
       newObject.text = sourceObject.title;
       newObject.label = sourceObject.label;
       newObject.globalStyles = globalStyles;
       newObject.field_name = sourceObject.name;
       newObject.element = sourceObject.element;
       newObject.sequence = sourceObject.sequence;
       newObject.field_varient = sourceObject.field_varient;

       newObject.canHaveOptionValue = true;
       newObject.canHaveAlternateForm = true;
       newObject.canHaveOptionCorrect = true;
       newObject.canPopulateFromApi = true;
       newObject.canHaveDisplayHorizontal = true;
       newObject.canHavePageBreakBefore = true;

       return newObject;
    }

    getSectionObject(item, globalStyles){
       var sectionHeader = new Object();
       sectionHeader = this.setCommonFields(sectionHeader, item, globalStyles);

       sectionHeader.required = false;
       sectionHeader.inline = undefined;
       sectionHeader.static = undefined;
       sectionHeader.showDescription = undefined;
       sectionHeader.globalStyles = globalStyles;

       return sectionHeader;
    }

    getText(item, globalStyles){
        var textObject = new Object();
        textObject = this.setCommonFields(textObject, item, globalStyles);

        textObject.isCharLimit = item.isCharLimit;
        textObject.canHaveAnswer = true;
        if(textObject.isCharLimit)
            textObject.charLimit = item.charLimit;

        return textObject;
    }
    getPhone(item, globalStyles){
        var textObject = new Object();
        textObject = this.setCommonFields(textObject, item, globalStyles);
        textObject.mask = item.isMasked;
        textObject.canHaveAnswer = true;

        return textObject;
    }
    getNumber(item, globalStyles){
        var numberObject = new Object();
        numberObject = this.setCommonFields(numberObject, item, globalStyles);
        numberObject.canHaveAnswer = true;
        
        return numberObject;
    }
    getTable(item, globalStyles){
        var tableObject = new Object();
        tableObject = this.setCommonFields(tableObject, item, globalStyles);
        tableObject.rows = item.rows;
        tableObject.headerList = item.headerList;
        tableObject.canHaveAnswer = true;
        
        return tableObject;
    }


   render() {
     return (
        <button className="btn btn-default float-right" style={{ marginRight: '10px' }} onClick={() => this.generateDataItems()}>Edit Form</button>
     );
   }
 }
 