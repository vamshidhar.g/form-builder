import React from 'react';
import store from './stores/store';

const generateJson = (props) => {
    console.log(props);
    let root = generateBasicStructureObj();
    let elements = props.data.data;
    console.log(elements);

    let pages = new Array();
    let pageCount = 0;
    let currentPage = generatePageObj(pageCount);
    let currentSection = null;
    let sectionCount = 0;

    for (let index = 0; index < elements.length; index++) {
        switch(elements[index].element){
            case 'MaterialText':
                let textElement = generateMaterialTextObj(index, elements[index]);
                if(currentSection != null){
                    currentSection.elements.push(textElement);
                }else{
                    currentPage.elements.push(textElement);
                }
                break;
            case 'MaterialPhone':
                let phoneElement = generateMaterialPhoneObj(index, elements[index]);
                if(currentSection != null){
                    currentSection.elements.push(phoneElement);
                }else{
                    currentPage.elements.push(phoneElement);
                }
                break;
            case 'MaterialNumber':
                let numberElement = generateMaterialNumberObj(index, elements[index]);
                if(currentSection != null){
                    currentSection.elements.push(numberElement);
                }else{
                    currentPage.elements.push(numberElement);
                }
                break;
            case 'SectionHeader':
                if(currentSection != null){
                    currentPage.sections.push(currentSection);
                    sectionCount++;
                }
                currentSection = generateSectionHeaderObj(sectionCount, elements[index]);
                break;
            case 'MaterialTable':
                let tableElement = generateInputTableObj(index, elements[index]);
                if(currentSection != null){
                    currentSection.elements.push(tableElement);
                }else{
                    currentPage.elements.push(tableElement);
                }
                break;
            default:
                break;
        }

        if(elements[index].element.hasOwnProperty("globalStyles"))
            root.styles = elements[index].element.globalStyles;
    }

    if(currentSection != null){
        currentPage.sections.push(currentSection);
    }

    pages.push(currentPage);
    root.pages = pages;

    console.log(JSON.stringify(root));
    try{
        localStorage.setItem('FORM_JSON', JSON.stringify(root));
        alert("Generated the json and stored successfully");
        store.dispatch('updateOrder', []);
        props.closePreview();
    }catch(e){
        alert("Error generating the JSON");
    }
    

    
}

const generateMaterialTextObj = (sequence, field) => {
    // return {};
    let object = new Object();
    
    object.sequence = sequence;
    object.id = field.id;
    object.name = field.field_name;
    object.label = field.label;
    object.field_varient = field.field_varient;
    object.title = field.text;
    object.element = field.element;

    let options = new Object();
    options.required = field.required;
    options.isCharLimit = field.hasOwnProperty("isCharLimit") ? field.isCharLimit : false;
    if(options.isCharLimit)
        options.charLimit = field.charLimit;
    object.options = options;

    return object;
}
const generateMaterialPhoneObj = (sequence, field) => {
    // return {};
    let object = new Object();

    object.sequence = sequence;
    object.id = field.id;
    object.name = field.field_name;
    object.label = field.label;
    object.field_varient = field.field_varient;
    object.title = field.text;
    object.element = field.element;


    let options = new Object();
    options.required = field.required;
    options.isMasked = field.hasOwnProperty("mask") ? field.mask : false;
    object.options = options;

    return object;
}
const generateMaterialNumberObj = (sequence, field) => {
    // return {};
    let object = new Object();

    object.sequence = sequence;
    object.id = field.id;
    object.name = field.field_name;
    object.label = field.label;
    object.field_varient = field.field_varient;
    object.title = field.text;
    object.element = field.element;

    let options = new Object();
    options.required = field.required;
    object.options = options;

    return object;
}
const generateSectionHeaderObj = (sequence, field) => {
    // return {};
    let object = new Object();

    object.sequence = sequence;
    object.id = field.id;
    object.name = field.field_name;
    object.label = field.label;
    object.field_varient = field.field_varient;
    object.title = field.text;
    object.element = field.element;
    object.elements = new Array();

    return object;
}

const generateInputTableObj = (sequence, field) => {
    // return {};

    let object = new Object();

    object.sequence = sequence;
    object.id = field.id;
    object.name = field.field_name;
    object.label = field.label;
    object.field_varient = field.field_varient;
    object.title = field.text;
    object.element = field.element;
    object.headerList = field.headerList;
    object.rows = field.rows;


    let options = new Object();
    options.required = field.required;
    object.options = options;

    return object;
}

const generateBasicStructureObj = () => {
    let json = new Object();
    json["name"] = "Text form";
    json["id"] = 123;
    json["description"] = ""
    json["styles"] = null;

    return json;
}

const generatePageObj = sequence => {
    let page = new Object();
    page.sequence = sequence;
    page.elements = new Array();
    page.sections = new Array();

    return page;
}

export default function GenerateJson(props) {
    return (
        <a href="#" className='btn btn-default btn-cancel btn-big' onClick={()=>generateJson(props)}>Generate JSON</a>
    )
}