import React from 'react';
import { makeStyles, createMuiTheme } from '@material-ui/core/styles';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, TextField,Button, ButtonGroup, Grid } from '@material-ui/core';
import ComponentHeader from '../form-elements/component-header';
import ComponentLabel from '../form-elements/component-label';
import { green } from '@material-ui/core/colors';
import store from '../stores/store';

const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 450,
  },
  input: {
      minWidth: 10,
      width: '25ch',
  },
  margin: {
    margin: theme.spacing(1),
  }
}));

const theme = createMuiTheme({
    palette: {
      primary: green,
    },
  });

function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

const rows = [
  createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
  createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
  createData('Eclair', 262, 16.0, 24, 6.0),
  createData('Cupcake', 305, 3.7, 67, 4.3),
  createData('Gingerbread', 356, 16.0, 49, 3.9),
];


const TextInputCells = props => {
    const classes = useStyles();
    // console.log(props);
    return (
        <TextField
            size="small" 
            required={props.required}
            variant={props.fieldVarient} />
    );
}

rows.map((row, index) => (
    <TableRow key={row}>
        <TableCell component="th" scope="row" align="left"><TextInputCells id={row+''+index} /></TableCell>
    </TableRow>
))

const MyTable = props => {
    const classes = useStyles();
    return(
        <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        {props.tableHeaders.map((header, index) => (
                            <TableCell align="left" key={index}><b>{header.label + (header.required ? " *" : "")}</b></TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {/* <GetRows rows={props.rows} /> */}
                    {
                        props.rows.map((row, rowIndex) => (
                            <TableRow key={row.rowId}>
                                {props.tableHeaders.map((header, cellIndex) => (
                                    <TableCell key={row+cellIndex} component="th" scope="row" align="left">
                                        <TextInputCells required={header.required} fieldVarient={props.fieldVarient} id={row+''+rowIndex} />
                                    </TableCell>
                                ))}
                            </TableRow>
                        ))
                    }
                </TableBody>
            </Table>
        </TableContainer>
    )
}

class MaterialTable extends React.Component {
    constructor(props){
        super(props);

        const { onLoad, onPost } = props;
        store.setExternalHandler(onLoad, onPost);

        this.state = {
            data: [props.data],
            headerList: [
                {label: 'Header Item 1', headerId: "header1", required: false}
                // {name: 'Calories'},
                // {name: 'Fat&nbsp;(g)'},
                // {name: 'Carbs&nbsp;(g)'},
                // {name: 'Protein&nbsp;(g)'},
            ],
            rows: [
                {rowId: 0}
            ]
        }
    }

    addRow(){
        console.log("Add Row");
        let data = this.state.data;
        let rows = data[0].hasOwnProperty('rows') ? data[0].rows : this.state.rows;

        let newRow = rows[0];
        newRow.rowId = rows.length;

        rows.push(newRow);
        data[0].rows = rows;
        store.dispatch('updateOrder', data);
    }
    deleteRow(){
        console.log("Delete Row");
    }

    render(){
        let baseClasses = 'SortableItem rfb-item';
        if (this.props.data.pageBreakBefore) { baseClasses += ' alwaysbreak'; }

        let propsData = this.props.data;
        let headerList = propsData.hasOwnProperty('headerList') ? propsData.headerList : this.state.headerList;
        let rows = propsData.hasOwnProperty('rows') ? propsData.rows : this.state.rows;

        const fieldVarient = (!this.props.globalStyles.form_default && propsData.hasOwnProperty("field_varient")) ? propsData.field_varient : this.props.globalStyles.global_field_varient;
        const CHARACTER_LIMIT = (propsData.isCharLimit && propsData.charLimit > 0) ? propsData.charLimit : 0;
        const formPreview = this.props.hasOwnProperty('isFormPreview') ? this.props.isFormPreview : false;
        
        return (
            <div className={baseClasses}>
                <ComponentHeader {...this.props} />
                <div className="form-group">
                    { !formPreview && <ComponentLabel {...this.props} /> }

                    <>
                        <MyTable tableHeaders={headerList} rows={rows} fieldVarient={fieldVarient} />
                        {/* <Button onClick={this.addRow.bind(this)} color="primary">Add Row</Button>
                        <Button onClick={this.deleteRow.bind(this)} color="secondary">Delete Row</Button> */}
                    </>
                </div>
            </div>
        );
    }
}

export default MaterialTable;