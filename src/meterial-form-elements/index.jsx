import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Button, TextField, Grid, Card, CardContent, Typography } from '@material-ui/core';
import ComponentHeader from '../form-elements/component-header';
import ComponentLabel from '../form-elements/component-label';
import MaterialTable from './tableComponent';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backgroundColor: "#202020",
        height: 50,
        padding: '0px'
    },
    cardcontent: {
        padding: '11px',
        align: "center"
    },
    typography: {
        flexGrow: 1,
        textAlign: "center",
        color: "#e5e5e5",
        fontSize: 20,
    }
}));

class MaterialText extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            inputText: ""
        }
        this.inputField = React.createRef();
    }

    textChange = event => {
        let value = event.target.value;
        console.log(value);

        if(this.props.data.isCharLimit)
            if(value.length > this.props.data.charLimit)
                return;

        this.setState({inputText: value});            
    }

    render() {
        // const props = {};
        // props.type = 'text';
        // props.className = 'form-control';
        // props.name = this.props.data.field_name;
        // if (this.props.mutable) {
        //     props.defaultValue = this.props.defaultValue;
        //     props.ref = this.inputField;
        // }

        let baseClasses = 'SortableItem rfb-item';
        if (this.props.data.pageBreakBefore) { baseClasses += ' alwaysbreak'; }

        // if (this.props.read_only) {
        //     props.disabled = 'disabled';
        // }
        
        const propsData = this.props.data;
        const fieldVarient = (!this.props.globalStyles.form_default && propsData.hasOwnProperty("field_varient")) ? propsData.field_varient : this.props.globalStyles.global_field_varient;
        const CHARACTER_LIMIT = (propsData.isCharLimit && propsData.charLimit > 0) ? propsData.charLimit : 0;

        let inputProps = {};
        if(CHARACTER_LIMIT){ 
            inputProps = {
                maxlength: CHARACTER_LIMIT
            }
        }
        if(this.props.mutable){
            inputProps.ref = this.inputField;
        }

        const formPreview = this.props.hasOwnProperty('isFormPreview') ? this.props.isFormPreview : false;

        return (
            <div className={baseClasses}>
                <ComponentHeader {...this.props} />
                <div className="form-group">
                    { !formPreview && <ComponentLabel {...this.props} /> }
                    <TextField    
                        fullWidth 
                        size="small" 
                        id={this.props.id}
                        variant={fieldVarient} 
                        password={propsData.mask}
                        label={propsData.mask ? "#######" : propsData.label} 
                        required={propsData.required}
                        name={this.props.data.field_name} 
                        inputProps={inputProps}
                        value={this.state.inputText}
                        helperText={(CHARACTER_LIMIT > 0) ? "Character limit is "+CHARACTER_LIMIT : ""}
                        onChange={this.textChange.bind(this)}
                    />
                </div>
            </div>
        );
    }
}

class MaterialPhone extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            phone: '',
            areaCode: ''
        }
        this.inputField = React.createRef();
    }

    phoneChange = event => {
        console.log(event.target.value);
        
        let numberPattern = /^(\s*|\d+)$/;
        let value = event.target.value.replaceAll("-", "");

        if(value.match(numberPattern)){
            if(this.props.data.mask){
                if(value.length > 10)
                    return;

                if(value.length > 3){
                    value = value.slice(0, 3) + "-" + value.slice(3);
                } 
                if(value.length > 7){
                    value = value.slice(0, 7) + "-" + value.slice(7);
                }
                this.setState({phone: value});   
            }else{
                if(value.length > 7)
                    return;

                if(value.length > 3){
                    value = value.slice(0, 3) + "-" + value.slice(3);
                } 
                this.setState({phone: value});   
            }
        }else{
            return;
        }
    }

    areaCodeChange = event => {
        let value = event.target.value;
        console.log(value);

        let numberPattern = /^(\s*|\d+)$/;
        if(value.match(numberPattern) && value.length <= 3){
            this.setState({areaCode: value});
        }
    }

    render() {
        // const props = {};
        // props.type = 'text';
        // props.className = 'form-control';
        // props.name = this.props.data.field_name;
        // if (this.props.mutable) {
        //     props.defaultValue = this.props.defaultValue;
        //     props.ref = this.inputField;
        // }

        let baseClasses = 'SortableItem rfb-item';
        if (this.props.data.pageBreakBefore) { baseClasses += ' alwaysbreak'; }

        // if (this.props.read_only) {
        //     props.disabled = 'disabled';
        // }
        // const placeholder = "dfjsdbhfbj";
        const phone = this.state.phone;
        const areaCode = this.state.areaCode;

        const propsData = this.props.data;
        const fieldVarient = (!this.props.globalStyles.form_default && propsData.hasOwnProperty("field_varient")) ? propsData.field_varient : this.props.globalStyles.global_field_varient;

        const formPreview = this.props.hasOwnProperty('isFormPreview') ? this.props.isFormPreview : false;
        return (
            <div className={baseClasses}>
                <ComponentHeader {...this.props} />
                <div className="form-group">
                    { !formPreview && <ComponentLabel {...this.props} /> }

                    <Grid container direction={"row"} spacing={5}>
                        {!propsData.mask && 
                            <Grid item>
                                <TextField      
                                    size="small" 
                                    id={this.props.id}
                                    variant={fieldVarient} 
                                    password={propsData.mask}
                                    label="Area Code" 
                                    width="3ch"
                                    value={areaCode}
                                    required={propsData.required}
                                    name={this.props.data.field_name} 
                                    onChange={this.areaCodeChange.bind(this)}
                                />
                            </Grid>
                        }
                        <Grid item>
                            <TextField    
                                size="small" 
                                id={this.props.id}
                                variant={fieldVarient} 
                                password={propsData.mask}
                                label={propsData.label} 
                                required={propsData.required}
                                name={this.props.data.field_name}
                                value={phone}
                                onChange={this.phoneChange.bind(this)}
                                helperText={propsData.mask ? "000-000-0000" : ""}
                            />
                        </Grid>
                    </Grid>
                </div>
            </div>
        );
    }
}

class MaterialNumber extends React.Component {
    constructor(props) {
        super(props);
        this.inputField = React.createRef();
        this.state = {
            number: ''
        }
    }

    numberChange = event => {
        let value = event.target.value;
        console.log(value);

        let numberPattern = /^(\s*|\d+)$/;
        if(value.match(numberPattern))
            this.setState({number: value});
    }

    render() {
        // const props = {};
        // props.type = 'number';
        // props.className = 'form-control';
        // props.name = this.props.data.field_name;

        // if (this.props.mutable) {
        //     props.defaultValue = this.props.defaultValue;
        //     props.ref = this.inputField;
        // }

        // if (this.props.read_only) {
        //     props.disabled = 'disabled';
        // }

        let baseClasses = 'SortableItem rfb-item';
        if (this.props.data.pageBreakBefore) { baseClasses += ' alwaysbreak'; }

        // if (this.props.read_only) {
        //     props.disabled = 'disabled';
        // }
        

        const propsData = this.props.data;
        const fieldVarient = (!this.props.globalStyles.form_default && propsData.hasOwnProperty("field_varient")) ? propsData.field_varient : this.props.globalStyles.global_field_varient;

        const formPreview = this.props.hasOwnProperty('isFormPreview') ? this.props.isFormPreview : false;
        return (
            <div className={baseClasses}>
                <ComponentHeader {...this.props} />
                <div className="form-group">
                    { !formPreview && <ComponentLabel {...this.props} /> }

                    <TextField
                        fullWidth 
                        size="small"
                        type="number"
                        variant={fieldVarient}
                        id={this.props.id}
                        label={propsData.label}
                        required={propsData.required}
                        name={this.props.data.field_name} 
                        value={this.state.number}
                        onChange={this.numberChange.bind(this)}
                        InputProps={{ inputProps: { min: 0} }}
                    />
                </div>
            </div>
        );
    }
}

const SectionHeader = (props) =>  {
    const classes = useStyles();

    let baseClasses = 'SortableItem rfb-item';
    if (props.data.pageBreakBefore) { baseClasses += ' alwaysbreak'; }

    if (props.read_only) {
        props.disabled = 'disabled';
    }
    const formPreview = props.hasOwnProperty('isFormPreview') ? props.isFormPreview : false;
    return (
        <div className={baseClasses}>
            <ComponentHeader {...props} />
            <div className="form-group">
                { !formPreview && <ComponentLabel {...props} /> }
                <Card className={classes.root}>
                    <CardContent className={classes.cardcontent}>
                        <Typography className={classes.typography} variant="h5" component="h2">
                            <b>{props.data.label}</b>
                        </Typography>
                    </CardContent>
                </Card>
            </div>
        </div>
    );
}



const FormElements = {};

FormElements.MaterialText =  MaterialText;
FormElements.MaterialPhone = MaterialPhone;
FormElements.SectionHeader = SectionHeader;
FormElements.MaterialTable = MaterialTable;
FormElements.MaterialNumber = MaterialNumber;

export default FormElements;